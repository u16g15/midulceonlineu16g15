const agregar = (req, res) => {
    console.log('metodo agregar');
}

const listar = (req, res) => {
    console.log('metodo listar');
}

const eliminar = (req, res) => {
    console.log('metodo eliminar');
}

const editar = (req, res) => {
    console.log('metodo editar');
}

const listarUno = (req, res) => {
    console.log('metodo listarUno');
}

export {
    agregar,
    listar,
    eliminar,
    editar,
    listarUno

}
