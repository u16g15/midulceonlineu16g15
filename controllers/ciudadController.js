import Ciudad from "../models/Ciudad.js";

const agregar = async (req, res) => {
    try {
        const ciudad = new Ciudad(req.body);
        const ciudadAlmacenada = await ciudad.save();
        res.json({ body: ciudadAlmacenada, ok: "SI"})
    } catch (error) {
        console.log(error);
    }
}

const listar = async (req, res) => {
    console.log('metodo listar');
}

const eliminar = async (req, res) => {
    console.log('metodo eliminar');
}

const editar = async (req, res) => {
    console.log('metodo editar');
}

const listarUno = async (req, res) => {
    console.log('metodo listarUno');
}

export {
    agregar,
    listar,
    eliminar,
    editar,
    listarUno

}
