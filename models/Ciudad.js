import mongoose from "mongoose";

const ciudadSchema = mongoose.Schema({
    nombreCiudad: {
        type: String,
        require: true,
        trim: true
    },

    estadoCiudad: {
        type: Number,
        require: true,
        trim: true
    }
}, {
    timestamps: true
});

const Ciudad = mongoose.model("Ciudad", ciudadSchema);
export default Ciudad;