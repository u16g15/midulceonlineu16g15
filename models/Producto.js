import mongoose, { NativeBuffer } from "mongoose";

const productoSchema = mongoose.Schema({
    idCategoria: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Categoria",
        require: true,
        trim: true
    },

    nombreProducto: {
        type: String,
        require: true,
        trim: true
    }, 

    referenciaProducto: {
        type: String,
        require: true,
        trim: true
    }, 

    precioVenta: {
        type: Number,
        require: true,
        trim: true
    }, 

    descripcionProducto: {
        type: String,
        require: true,
        trim: true
    }, 

    cantidadProducto: {
        type: Number,
        require: true,
        trim: true
    }, 

    imagenCodificada: {
        type: String,
        require: true,
        trim: true
    }, 

    unidadMedida: {
        type: Number,
        require: true,
        trim: true
    }, 

    estadoProducto: {
        type: Number,
        require: true,
        trim: true
    }
}, {
    timestamps: true
});

const Producto = mongoose.model("Producto", productoSchema);
export default Producto;