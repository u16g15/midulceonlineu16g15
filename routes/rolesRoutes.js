import express, { Router } from "express";
const router = express.Router();
import { agregar, listar, eliminar, editar, listarUno } from '../controllers/rolController.js';
import validarAutenticacion from "../middleware/validarAutenticacion.js"

router.post("/", validarAutenticacion, agregar);
router.get("/", validarAutenticacion, listar);
router.delete("/:id", validarAutenticacion, eliminar);
router.put("/:id", validarAutenticacion, editar);
router.get("/:id", validarAutenticacion, listarUno);

export default router;