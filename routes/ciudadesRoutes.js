import express, { Router } from "express";
const router = express.Router();
import { agregar, listar, eliminar, editar, listarUno } from '../controllers/ciudadController.js';

router.post("/", agregar);
router.get("/", listar);
router.delete("/", eliminar);
router.put("/", editar);
router.get("/:id", listarUno);


export default router;